package service.java.quotes;

import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


@RestController
@Slf4j
public class QuotesController {
    private static final String template = "Hello, %s!";
    private final QuotesRepository repository;

    QuotesController(QuotesRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/quotes")
    List<Quotes> all() {
        return repository.findAll();
    }

    @PostMapping("/quotes")
    Quotes newEmployee(@RequestBody Quotes newQuotes) {
        return repository.save(newQuotes);
    }

    @GetMapping("/quotes/{id}")
    Quotes one(@PathVariable Long id) {

        return repository.findById(id)
                .orElseThrow(() -> new QuotesNotFoundException(id));
    }

    @PutMapping("/quotes/{id}")
    Quotes replaceQuotes(@RequestBody Quotes newQuotes, @PathVariable Long id) {

        return repository.findById(id)
                .map(quotes -> {
                    quotes.setGuid(newQuotes.getGuid());
                    quotes.setLink(newQuotes.getLink());
                    quotes.setTitle(newQuotes.getTitle());
                    quotes.setDescription(newQuotes.getDescription());
                    quotes.setPubDate(newQuotes.getPubDate());
                    return repository.save(quotes);
                })
                .orElseGet(() -> {
                    newQuotes.setId(id);
                    return repository.save(newQuotes);
                });
    }

    @DeleteMapping("/quotes/{id}")
    void deleteQuotes(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
