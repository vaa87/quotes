package service.java.quotes;

class QuotesNotFoundException extends RuntimeException {

    QuotesNotFoundException(Long id) {
        super("Could not find employee " + id);
    }
}