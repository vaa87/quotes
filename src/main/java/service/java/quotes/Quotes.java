package service.java.quotes;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Quotes {
    private @Id
    @GeneratedValue
    Long id;

    private String guid;

    private String link;

    private String title;

    private String description;

    private Date pubDate;

    Quotes() {

    }

    Quotes(String guid, String link, String title, String description, Date pubDate) {
        this.guid = guid;
        this.link = link;
        this.title = title;
        this.description = description;
        this.pubDate = pubDate;
    }

    public Long getId() {
        return id;
    }

    public String getGuid() {
        return guid;
    }

    public String getLink() {
        return link;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Date getPubDate() {
        return pubDate;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }
}
