package service.java.quotes;

import org.springframework.data.jpa.repository.JpaRepository;

interface QuotesRepository extends JpaRepository<Quotes, Long> {

}
