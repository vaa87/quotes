package service.java.quotes;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Configuration
@Slf4j
class LoadDatabase {

    @Bean
    CommandLineRunner initDatabase(QuotesRepository repository) {
        return args -> {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);
            log.info("Preloading " + repository.save(new Quotes("1", "234534", "@3434", "ew34w5fer", dateFormat.parse("Fri, 13 Sep 2019 13:46:27 +0400"))));
            log.info("Preloading " + repository.save(new Quotes("2", "234534", "@33454", "ew345fer", dateFormat.parse("Fri, 13 Sep 2019 13:46:27 +0400"))));
        };
    }
}